'''
Created on Mar 12, 2015

@author: daniel
'''
from DLoader.Plugins import Modules
from DLoader.Validators import PHValidator, TemperatureValidator, ConductivityValidator

class FormatModule(Modules):
    '''
    classdocs
    '''


    def __init__(self,formatID):
        '''
        Constructor
        '''
        #Setting the formatID. This is required.
        #Note that the method is in the parent class, Modules
        Modules.setDescription(formatID)
        
    def parseData(self, data):
        data=data.lstrip()
        data=data.split()
#split into data and hmac by column 
        resultDict={}#dictionary to store json data
        resultDict['locid']=data[0]+data[1][:3];
        resultDict['keyid']=data[1][3:6];
        resultDict['formatid']=data[1][6:10];
        resultDict['datetime']=data[1][10:20]+' '+data[2][:13];
        resultDict['temperature']=float(data[2][13:]+data[3]);
        resultDict['pH']=float(data[4]);
        #resultDict['pH']=PHValidator()
        if resultDict['pH'] <=5:
            resultDict['pH']=resultDict['pH'];
        elif resultDict['pH']<=8:
            resultDict['pH']=float(resultDict['pH'])+1;
        else:
            resultDict['pH']=9.999;                  
        resultDict['conductivity']=float(data[5][0:6]);
        resultDict['hmac']=data[5][6:];
        
        pass
    
