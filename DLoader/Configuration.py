'''
Created on Mar 11, 2015

@author: daniel
'''
import os
import logging
import ConfigParser
import importlib
import copy
from datetime import datetime




#This calss is going to hold configuration methods
#and also load all configs required for smooth operation of the script
class Configuration:
    BASE_DIR=os.path.dirname(os.path.dirname('__file__'))
    CONFIGFILE=os.path.join(BASE_DIR,"settings.cfg")
    KEYFILE=os.path.join(BASE_DIR,"keys.cfg") #this is where all keys will be stored
    #Taking the FORMATFILE out so that we just give the names of the modules in the settings.
    #Actual implementation is going to be within plugins
    #FORMATFILE=os.path.join(BASE_DIR,"formats.cfg")
    
    #Declaring essential variables
    LOG_DIR=""
    TEMP_DIR=""
    INPUT_DIR=""
    BACKUP_DIR=""
    FORMATS={}#Dictionary that stores format objects using their IDs as Keys

    
    LOG_LEVEL={"DEBUG":logging.DEBUG,
               "CRITICAL":logging.CRITICAL,
               "INFO":logging.INFO,
               "WARNING":logging.WARNING }
    
    CONFIG_SECTIONS={
                 "logs":"logs",
                 "dir":"directories",
                 "keyids":"keys",
                 "formatids":"formats"
                 }

    
    configparser=ConfigParser.ConfigParser(allow_no_value=True)
    
    
    #Checking for the existence of configuration file
    #If it does not exist we have to log and quit the script 
    #immediately
    def __init__(self):
        if  not os.path.exists(self.CONFIGFILE):
            logging.critical("Configuration file, %s, settings.cfg does cannot be found" %self.CONFIGFILE )
            logging.critical("Exiting script")
            quit
        
        
        self.configparser.read([self.CONFIGFILE])
        self.LOG_DIR=self.getConfig(self.CONFIG_SECTIONS["logs"], "path")
        logLevelKey=self.getConfig(self.CONFIG_SECTIONS["logs"], "level")
        logLevelKey= logLevelKey  if (logLevelKey in self.LOG_LEVEL)  else "DEBUG" #Validating the level from config file
        #else we are using default value
        self.FORMATS=self.getFormats()
        
        #Configuring logging by the script with the appropriate 
        #filename and logging threshold provided in the configuration file
        logging.basicConfig(filename=os.path.join(self.LOG_DIR,
                                                  "loadDB"+datetime.now().strftime("%m%d%Y")+".log"),
                                                  level=logLevelKey)
        
        print self.configparser.items("logs")
        
        
        
        
        #Configuring log
    
    def getConfig(self,section,option):
        try:
            return self.configparser.get(section,option)
            
        except ConfigParser.NoOptionError:
            logging.critical("The section, %s, has not been configured in %s" %(section,self.CONFIGFILE))
            return ""
    
    #Method to pull details of format using format IDs
    #and organize them in a dictionary
    def getFormats(self):
        formatstr=self.getConfig(self.CONFIG_SECTIONS["formatids"],"plugins")
        format=formatstr.split(',')
        obj={}
        for curr in format:
            tmpobj=importlib.import_module("."+curr,package="plugins").FormatModule()
            obj[tmpobj.getID]=copy.deepcopy(tmpobj)
            
        return obj
    #We could encrypt and decrypt keys before reading
    def getKeys(self): 
        #Todo: This method could be modified to factor 
        #encryption of the key file. Thus decrypt befor read
        keyfileparser=ConfigParser.ConfigParser(allow_no_value=True)
        keyfileparser.read([self.KEYFILE])
        sections= keyfileparser.sections()
        keys={}
        for section in sections:
            keys[section]=keyfileparser.get(section, "key")
        return keys
    
    